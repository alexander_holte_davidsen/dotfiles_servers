SHELL := /bin/sh
all:
	@echo "install and uninstall are supported options"
install:
	@install -h md5 -m 0755 -C -g wheel -o davalex dot_cshrc /usr/home/davalex/.cshrc ;\

uninstall:
	@rm /usr/home/davalex/.cshrc
